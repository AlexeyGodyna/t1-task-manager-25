package ru.t1.godyna.tm.command.task;

import org.jetbrains.annotations.NotNull;

public final class TaskClearCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-clear";

    @NotNull
    private final String DESCRIPTION = "Remove all tasks.";

    @Override
    public void execute() {
        System.out.println("[TASKS CLEAR]");
        getTaskService().removeAll(getUserId());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
